# TyndaleOne Images

This repository is for the images required for the Unified ENGAGE project for Tyndale University College & Seminary. The initial reference documentation is included in the logos-4-tyndale-one.pdf file.

**This repository including all requested images is available as a single download (ZIP) in the sidebar (left) at <https://bitbucket.org/tyndalewebteam/tyndaleone-images/>**

---

## Image Details, Format and Location

* All images are stored in the /images folder and organized by type
* The logo-based images are provided in the requested PNG format as well as a vector-based SVG format 
* The Logo image is provided in both color and white versions (not sure what will work best in context)
* The Branding image is provided in color, dark and white versions (not sure what will work best in context)
* The App Icon is provided in both color and inverted versions (not sure what will work best for the brand)
* Background images are provided as JPG files in fullsize and minified (file size) versions
* The original Sketch working file is also provided

### Folder Structure

/images    
/images/1-logo    
/images/2-branding-image    
/images/3-app-icon    
/images/background-images    
/images/background-images/minified

---

## Images Required | Outline

1. **LOGO:** Provide the Logo in PNG format which would be used for creating the App Icon and Splash Screen for the mobile apps. Recommended Size: 140 x 140 px
2. **BRANDING IMAGE:** Provide the Branding Image in PNG format which would be used in the about Institution page within the mobile app. You can also refer to image on previous page. Recommended Size: 600 x 200 px
3. **APP ICON:** Please provide us the main app icons in PNG format. Size: 1024 x 1024
4. **BACKGROUND IMAGE**
  a. Portal: 2732 x 2732 px
  b. Mobile app:
    1. 2732 x 2732 px (Splash Landscape)
    2. 1278 x 2732 px (Splash Portrait)
    3. 2732 x 1278 px (Splash Portrait (wide?)) 
    4. 640 x 555 px (home page)